package pe.com.cine.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.com.cine.entity.Pelicula;

public interface PeliculaDao extends JpaRepository<Pelicula, Integer> {
	@Query("select p from Pelicula p where p.idPelicula=?1")
	Pelicula buscarPeliculaporCodigo(String idPelicula);

	/*
	 * @Query("select p from Pelicula p where p.genero.idGenero=?1") Pelicula
	 * buscarPeliculaporGenero(String idGenero);
	 */

	@Query("select p from Pelicula p where p.genero.idGenero = :idGenero")
	List<Pelicula> buscarPeliculaporGenero(@Param("idGenero") String idGenero);
}
