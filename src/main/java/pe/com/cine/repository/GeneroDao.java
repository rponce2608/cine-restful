package pe.com.cine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pe.com.cine.entity.Genero;

@Repository
public interface GeneroDao extends JpaRepository<Genero, Integer> {

	@Query("select g from Genero g where g.idGenero=?1")
	Genero buscarGeneroporCodigo(String idGenero);
}
