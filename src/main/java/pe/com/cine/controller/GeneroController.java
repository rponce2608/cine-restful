package pe.com.cine.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.cine.entity.Genero;
import pe.com.cine.services.IGeneroService;

@RestController
@RequestMapping("genero")
public class GeneroController {

	@Autowired
	private IGeneroService generoService;

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Genero>> findGenero() {
		return new ResponseEntity<List<Genero>>(generoService.find(null), HttpStatus.OK);
	}

	@GetMapping(value = "/listar/{idGenero}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Genero>> listarId(@PathVariable("idGenero") String idGenero) {
		List<Genero> lst=new ArrayList<>();
		try {
			lst =  generoService.find(idGenero);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Genero>>(lst, HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return new ResponseEntity<List<Genero>>(lst, HttpStatus.OK);
	}
}
