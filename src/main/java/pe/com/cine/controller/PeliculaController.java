package pe.com.cine.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.cine.entity.Pelicula;
import pe.com.cine.services.IPeliculaService;

@RestController
@RequestMapping("pelicula")
public class PeliculaController {

	@Autowired
	private IPeliculaService peliculaService;

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pelicula>> listar() {
		return new ResponseEntity<List<Pelicula>>(peliculaService.listarPeliculas(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/listarPorPelicula/{idPelicula}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pelicula>> listarPeliculaPorCodigo(@PathVariable("idPelicula") String idPelicula) {
		return new ResponseEntity<List<Pelicula>>(peliculaService.buscarPeliculaporCodigo(idPelicula), HttpStatus.OK);
	}

	@GetMapping(value = "/listarPorGenero/{idGenero}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pelicula>> listarPeliculaPorGenero(@PathVariable("idGenero") String idGenero) {
		return new ResponseEntity<List<Pelicula>>(peliculaService.buscarPeliculaporGenero(idGenero), HttpStatus.OK);	
	}
}
