package pe.com.cine.services;

import java.util.List;

import pe.com.cine.entity.Pelicula;

public interface IPeliculaService {

	List<Pelicula> buscarPeliculaporCodigo(String idPelicula);
	
	List<Pelicula> buscarPeliculaporGenero(String idGenero);
	
	List<Pelicula> listarPeliculas();

}
