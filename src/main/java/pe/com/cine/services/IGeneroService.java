package pe.com.cine.services;

import java.util.List;

import pe.com.cine.entity.Genero;

public interface IGeneroService {

	List<Genero> find(String id);

}
